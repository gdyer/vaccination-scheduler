Foreword Covid-19 vaccination scheduler is licensed under a `Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License <https://creativecommons.org/licenses/by-nc-sa/4.0/>`_.

This project may use other libraries which have their own licenses.
