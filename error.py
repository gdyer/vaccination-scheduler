# -*- coding: utf-8 -*-
"""
error.py
~~~~~~~

Proper error handling for RESTful API.
"""

class UsageError(Exception):
  status_code = 400

  def __init__(self, message, status_code=None, other={}):
    Exception.__init__(self)
    self.message = message
    self.other = other 
    if status_code is not None:
      self.status_code = status_code

  def to_dict(self):
    rev = {'message' : self.message, 'ok' : 0}
    rev.update(self.other)
    return rev

