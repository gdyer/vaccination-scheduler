var pfhApp = angular.module('pfhApp', ['ngRoute', 'ngSanitize', 'ngCookies', 'angular-loading-bar']);
var ADD_PERSON = '/add_person';
var GET_TO_CONTACT = '/get_to_contact';
var SAVE_FROM_CONTACT = '/save_contact';
var DELETE = '/delete';
var GET_WAITING = '/get_waiting';
var GET_AVAIL_DATES = '/open_dates';
var SAVE_TIME = '/save_time';
var GET_SCHEDULED = '/get_scheduled'
var MOVE_TO_WAITING = '/move_to_waiting'
var MOVE_BACK_TO_WAITING = '/unschedule'
var CHANGE_INFO = '/change_info'
var RECORD_DOSE = '/record_dose'
var GET_VACCINATED = '/get_vaccinated'
var UNSET_DOSE= '/unset_dose'

pfhApp.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
	cfpLoadingBarProvider.includeSpinner = false;
}]);


pfhApp.run(['$rootScope', '$location', '$anchorScroll', '$window', '$cookies', '$timeout',
  function($rootScope, $location, $anchorScroll, $window, $cookies, $timeout) {
  $anchorScroll.yOffset = 82;

  $rootScope.pageSize = 12;

  $rootScope.cookies = {};
  function updateAlerts() {
    var i;
    for (i = 0; i < ALERTS.length; i++) {
      $rootScope.cookies[ALERTS[i]] = $cookies[ALERTS[i]] !== undefined;
    };
  }

  updateAlerts();
  $rootScope.dismissAlert = function(name) {
    $cookies[name] = '1';
    updateAlerts();
  };

  $rootScope.saveFormat = function() {
    $rootScope.formatIndex = ($rootScope.formatIndex===1) ? 0 : 1;
    $cookies.formatIndex = $rootScope.formatIndex;

    if ($rootScope.formatIndex === 0) {
      // International
      $rootScope.dateFormat = 'EEE., d MMM yyyy, HH:mm';
    } else {
      // American ...
      $rootScope.dateFormat = 'EEE., MMM d yyyy, hh:mm a';
    }
  };


  $rootScope.formatIndex = 0;
  $rootScope.dateFormat = 'EEE., d MMM yyyy, HH:mm';
  var fi = $cookies.formatIndex;
  if (fi !== undefined) {
    fi = parseInt(fi)
    if (fi === 1) {
      $rootScope.saveFormat();
    }
  }

  function clearFilter() {
    // dismiss and reset filter
    $rootScope.searched = '';
    $rootScope.search = '';
    $rootScope.show_search = false;
  }

  function updateLists() {
    $rootScope.updateToContact();
    $rootScope.updateWaiting();
    $rootScope.updateScheduled();
    $rootScope.updateVaccinated();
  }

  $rootScope.toggleSearch = function(from_input) {
    if ($rootScope.show_search && $rootScope.search && $rootScope.search.length > 2) {
      if ($rootScope.searched && $rootScope.search === $rootScope.searched) {
        clearFilter();
      }
      updateLists();
    } else if ($rootScope.show_search && !($rootScope.search || $rootScope.search.length > 2)) {
      // should dismiss without searching
      $rootScope.search = '';
      $rootScope.show_search = !$rootScope.show_search;
      if ($rootScope.searched) {
        clearFilter()
        updateLists();
      }
    } else if ($rootScope.show_search && $rootScope.search.length <= 2) {
      $rootScope.show_search = false;
      if ($rootScope.searched) {
        clearFilter();
        updateLists();
      }
    } else if (!$rootScope.show_search) {
      // opening 
      $rootScope.filterDate = undefined;
      $rootScope.show_search = !$rootScope.show_search;
      $timeout(function() {
        // hacky
        document.getElementById('search_field').focus();
      }, 250);
    }
  };
}]);


pfhApp.controller('VaccinatedController', function($scope, $rootScope, $http, $anchorScroll, $timeout) {
  $scope.update = function() {
    $http.get(GET_VACCINATED + (($rootScope.search) ? ('?q=' + $rootScope.search) : ''))
      .success(function(res) {
        $scope.npeople = res.npeople;
        if ($rootScope.search && res.npeople > 0 && res.npeople < $rootScope.pageSize) {
          $scope.people = res.people;
        } else {
          $scope.people = undefined;
        }
      }).error(function(res) {
        $scope.updateError = 'We couldn’t update this list; please try refreshing the page.';
      });
  };

  $scope.update();
  $rootScope.updateVaccinated = $scope.update;


  $scope.unsetDose = function(p) {
    $http.post(UNSET_DOSE, p)
      .success(function(res) {
        $rootScope.updateWaiting();
        $rootScope.updateScheduled();
        $scope.update();
      }).error(function(res) {
        $scope.updateError = (res.message) ? res:  { ok: 0, message: 'the record couldn’t be updated; please try refreshing the page'};
    });
  };
});

pfhApp.controller('ScheduledController', function($scope, $rootScope, $http, $anchorScroll, $timeout) {
  $scope.firstJanuary = new Date('2021-01-02');

  $scope.clearFilterDate = function() {
    $scope.filteredDate = false;
    $scope.update();
  };
  $scope.update = function(goToPage) {
    $scope.todayDate = new Date();

    $rootScope.show_date_picker = false;

    var str = [];
    var params = {};
    if ($rootScope.search) {
      params.q = $rootScope.search;
    }
    if ($rootScope.filterDate) {
      var filterDate = $rootScope.filterDate;
      params.date = filterDate.getFullYear() + '-' + (filterDate.getMonth() + 1) + '-' + filterDate.getDate();
      params.q = '';
      $rootScope.search = '';
      $rootScope.searched = '';
      $rootScope.show_search = false;
    }
    for (var p in params) {
      str.push(encodeURIComponent(p) + '=' + encodeURIComponent(params[p]));
    }

    $http.get(GET_SCHEDULED + '?' + str.join('&'))
      .success(function(res) {
        $rootScope.searched = $rootScope.search;
        $scope.filteredDate = $rootScope.filterDate !== undefined;

        $scope.pages = res.people;
        $scope.npeople = res.npeople;
        if (goToPage !== undefined) {
          if (goToPage === $scope.pages.length) {
            $scope.page = goToPage - 1;
          }
          $scope.page = goToPage;
        } else {
          $scope.page = 0;
        }
        $scope.people = $scope.pages[$scope.page];

        $scope.result = undefined;
        $scope.updateError = undefined;

        if (res.first_date !== undefined) {
          $scope.min_filter_date = new Date(res.first_date);
        }



        $scope.flash_success = true;
        $timeout(function() {
          $scope.flash_success = false 
        }, 500);

        if (res.graph_data) {
          createGraph(res.graph_data, new Date(res.first_date), new Date(res.last_date));
        } else {
          hideGraph();
        }

      }).error(function(res) {
        $scope.updateError = 'We couldn’t update this list; please try refreshing the page.';
      });
  };


  $scope.changePage = function(i) {
    $scope.page += i;
    $scope.people = $scope.pages[$scope.page];
  };

  $scope.moveToWaiting = function(i) {
    $http.post(MOVE_BACK_TO_WAITING, $scope.people[i])
      .success(function(res) {
        $scope.update($scope.page);
        $rootScope.updateWaiting(res.highlight);
      }).error(function(res) {
        $scope.result = (res.message) ? res:  { ok: 0, message: 'the record couldn’t be updated; please try refreshing the page'};
    });

  };

  $scope.saveNewInfo = function(i) {
    $http.post(CHANGE_INFO, $scope.people[i])
      .success(function(res) {
        $scope.people[i] = res.person;
        $scope.people[i].expanded = true;
        $scope.people[i].new_name = $scope.people[i].name;
        $scope.people[i].new_phone = $scope.people[i].phone_national;

        $scope.people[i].show_change_name = true;
        $scope.change_result = { ok: 1, message: 'saved ✓' };
        $timeout(function() {
          $scope.people[i].show_change_name = false ;
          $scope.change_result = undefined;
        }, 1000);
      }).error(function(res) {
        $scope.change_result = (res.message) ? res : { ok: 0, message: 'failed to save' };
    });
  };

  $scope.recordDose = function(i) {
    $http.post(RECORD_DOSE, $scope.people[i])
      .success(function(res) {
        $scope.update($scope.page);
        $rootScope.updateVaccinated();
      }).error(function(res) {
        $scope.result = (res.message) ? res:  { ok: 0, message: 'the record couldn’t be updated; please try refreshing the page'};
    });
  };

  $scope.todayDate = new Date();
  $scope.update();
  $rootScope.updateScheduled = $scope.update;


  $scope.expand = function(i, p) {
    if (p.expanded) {
      $scope.people.splice(i+1, 1);
      $scope.people[i].confirm_delete = false;
    } else {
      $scope.people.splice(i+1, 0, {});
    }
    p.expanded = !p.expanded
  };


  $scope.deletePerson = function(i) {
    $http.post(DELETE, $scope.people[i])
      .success(function(res) {
        $scope.update($scope.page);
      }).error(function(res) {
        $scope.result = (res.message) ? res:  { ok: 0, message: 'the record couldn’t be deleted; please try refreshing the page'};
    });
  };

});


pfhApp.controller('WaitingController', function($scope, $rootScope, $http, $location, $anchorScroll, $timeout) {

  $scope.firstJanuary = new Date('2021-01-02');
  $scope.update = function(highlight) {
    $scope.todayDate = new Date();
    $http.get(GET_WAITING + (($rootScope.search) ? ('?q=' + $rootScope.search) : ''))
      .success(function(res) {
        $scope.updateError = undefined;

        $scope.page = 0;
        $scope.pages = res.people;
        $scope.npeople = res.npeople;
        $scope.people = $scope.pages[$scope.page];

        if (highlight && highlight.name !== undefined) {
          var i, j;
          for (i = 0; i < $scope.pages.length; i++) {
            for (j = 0; j < $scope.pages[i].length; j++) {
              var p = $scope.pages[i][j];
              if (p.name === highlight.name && p.phone === highlight.phone) {
                $scope.page = i;
                $scope.people = $scope.pages[$scope.page];
                $scope.expand(j, $scope.people[j]);

                var h = 'w' + j;
                if ($location.hash() !== h) {
                  $location.hash(h);
                }
                $anchorScroll();
              }
            }
          }
        } else {
          $scope.flash_success = true;
          $timeout(function() {
            $scope.flash_success = false 
          }, 500);
        }
      }).error(function(res) {
        $scope.updateError = 'We couldn’t update this list; please try refreshing the page.';
      });
  };

  $scope.todayDate = new Date();
  $scope.update();
  $rootScope.updateWaiting = $scope.update;

  $scope.changePage = function(i) {
    $scope.page += i;
    $scope.people = $scope.pages[$scope.page];
  };

  $scope.getAvailableDates = function(i) {
    var d = $scope.people[i].possible_day;
    if (!d) {
      clearForm(i);
      return;
    }
    $scope.people[i].nice_possible_day = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate();
    $http.post(GET_AVAIL_DATES, $scope.people[i])
      .success(function(res) {
        $scope.people[i].available_times = res.times;
        $scope.people[i].selected_time = undefined;
      }).error(function(res) {
        $scope.result = (res.message) ? res:  { ok: 0, message: 'problems updating available times; please try refreshing the page'};
      });
  };

  function clearForm(i) {
    if ($scope.people[i]) {
      $scope.people[i].available_times = undefined;
      $scope.people[i].possible_day = undefined;
      $scope.people[i].nice_possible_day = undefined;
      $scope.people[i].selected_time = undefined;
    }
  }

  $scope.saveTime = function(i, override_walk_in) {
    if (override_walk_in) {
      $scope.people[i].override_walk_in = true;
    }
    $http.post(SAVE_TIME, $scope.people[i])
      .success(function(res) {
        $scope.update();
        $rootScope.updateScheduled();
        $rootScope.updateVaccinated(); // for single-dose walk-ins
        $scope.result = res;

        if ($scope.people[i].walk_in) {
          $rootScope.lastUsedDate = $scope.people[i].possible_day;

          var h = 'add_person';
          if ($location.hash() !== h) {
            $location.hash(h);
          }
          $anchorScroll();
          $rootScope.toggleAddPerson();
        }

        clearForm(i);

        $timeout(function() {
          $scope.result = undefined;
        }, 2000);
      }).error(function(res) {
        $scope.result = (res.message) ? res:  { ok: 0, message: 'the record couldn’t be added; please try refreshing the page'};
      });
  };

  $scope.deletePerson = function(i) {
    $http.post(DELETE, $scope.people[i])
      .success(function(res) {
        $scope.update();
      }).error(function(res) {
        $scope.save_result = (res.message) ? res:  { ok: 0, message: 'the record couldn’t be deleted; please try refreshing the page'};
    });
  };


  $scope.expand = function(i, p) {
    if (p.expanded) {
      clearForm(i);
      $scope.people.splice(i+1, 1);
    } else {
      if (p.walk_in && $rootScope.lastUsedDate !== undefined) {
        p.possible_day = $rootScope.lastUsedDate;
        $scope.getAvailableDates(i);
      }
      $scope.people.splice(i+1, 0, {});
    }
    p.expanded = !p.expanded
  };
});


pfhApp.controller('ToContactController', function($scope, $rootScope, $http, $anchorScroll, $timeout, $location) {
  $scope.tc = {};
  $scope.update = function(highlight) {
    $scope.maxDate = new Date('2010-01-01');
    $scope.minDate = new Date('1900-01-01');
    $scope.todayDate = new Date();
    $http.get(GET_TO_CONTACT + (($rootScope.search) ? ('?q=' + $rootScope.search) : ''))
      .success(function(res) {
        $scope.updateError = undefined;

        $scope.page = 0;
        $scope.pages = res.people;
        $scope.npeople = res.npeople;
        $scope.people = $scope.pages[$scope.page];

        if (highlight && highlight.name !== undefined) {
          var i, j;
          for (i = 0; i < $scope.pages.length; i++) {
            for (j = 0; j < $scope.pages[i].length; j++) {
              var p = $scope.pages[i][j];
              if (p.name === highlight.name && p.phone === highlight.phone) {
                $scope.page = i;
                $scope.people = $scope.pages[$scope.page];
                $scope.expand(j, $scope.people[j]);

                var h = 'a' + j;
                if ($location.hash() !== h) {
                  $location.hash(h);
                }
                $anchorScroll();
              }
            }
          }
        } else {
          $scope.flash_success = true;
          $timeout(function() {
            $scope.flash_success = false 
          }, 500);
          $timeout(function() {
            $scope.result = undefined;
          }, 1600);
        }
      }).error(function(res) {
        $scope.updateError = 'We couldn’t update this list; please try refreshing the page.';
      });
  };

  $scope.update();
  $rootScope.updateToContact = $scope.update;

  $scope.changePage = function(i) {
    $scope.page += i;
    $scope.people = $scope.pages[$scope.page];
  };

  $scope.expand = function(i, p) {
    if (p.expanded) {
      $scope.people.splice(i+1, 1);
    } else {
      if (p.skin_colour == undefined) {
        p.skin_colour = 'none';
      }
      $scope.people.splice(i+1, 0, {});
    }
    p.expanded = !p.expanded
  };

  $scope.savePerson = function(i) {
    var d = $scope.people[i].dob;
    if (d) {
      $scope.people[i].nice_dob = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate();
    }
    $http.post(SAVE_FROM_CONTACT, $scope.people[i])
      .success(function(res) {
        $scope.show_add_person = false;
        $scope.update();
        $rootScope.updateWaiting(res.highlight);
      }).error(function(res) {
        $scope.save_result = (res.message) ? res:  { ok: 0, message: 'the record couldn’t be saved; please try refreshing the page'};
    });
  };

  $scope.deletePerson = function(i) {
    $http.post(DELETE, $scope.people[i])
      .success(function(res) {
        $scope.update();
      }).error(function(res) {
        $scope.save_result = (res.message) ? res:  { ok: 0, message: 'the record couldn’t be deleted; please try refreshing the page'};
    });
  };

  $scope.addPerson = function() {
    $http.post(ADD_PERSON, $scope.tc)
      .success(function(res) {
        $scope.show_add_person = false;
        $scope.result = res;
        $scope.tc = {};
        $rootScope.searched = '';
        $rootScope.search = '';
        $rootScope.show_search = false;
        $scope.update(res.highlight);
      }).error(function(res) {
        $scope.result = (res.message) ? res:  { ok: 0, message: 'the record couldn’t be added; please try refreshing the page'};
      });
  };


  $scope.toggleAddPerson = function() {
    if ($scope.show_add_person === true) {
      $scope.show_add_person = false;
    } else {
      $scope.show_add_person = true;
      if ($rootScope.lastUsedDate !== undefined) {
        $scope.tc.walk_in = true;
      }
      $timeout(function() {
        // hacky
        document.getElementById('tc_name').focus();
      }, 250);
    }
  };

  $rootScope.toggleAddPerson = $scope.toggleAddPerson;
});
