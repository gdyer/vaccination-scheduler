# -*- coding: utf-8 -*-
from config import SMTP_BOT_SENDER, SMTP_BOT_SENDER_PW, APP_NAME, REDIS_PORT, REDIS_HOST
import smtplib, ssl
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import db
import redis

SMTP_SERVER = 'mail.gandi.net'
SMTP_PORT = 465
MAIL_LOCK = 'mail_lock'
sr = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT)


def send_mail(recpt, subj, body, html):
  msg = MIMEMultipart('alternative')
  msg.set_charset('utf8')
  msg['Subject'] = '[%s] %s' % (APP_NAME, subj)
  msg['From'] = SMTP_BOT_SENDER
  msg['To'] = recpt

  text = '''%s

(This message was sent from an unmonitored address; please don't reply. Rather, contact info [at] foreword (dot) dev if this message is erroneous.)''' % body

  part1 = MIMEText(text, 'plain')
  part2 = MIMEText(html, 'html')
  msg.attach(part1)
  msg.attach(part2)

  context = ssl.create_default_context()
  with smtplib.SMTP_SSL(SMTP_SERVER, SMTP_PORT, context=context) as server:
    try:
      server.login(SMTP_BOT_SENDER, SMTP_BOT_SENDER_PW)
    except SMTPAuthenticationError:
      return False
    ret = True 
    try:
      server.sendmail(msg['From'], msg['To'], msg.as_string())
    except:
      ret = False
    finally:
      return ret


def enqueue_mail(recpt, subj, body, html):
  db.enqueue_mail(recpt, subj, body, html)



def dequeue_mail(pool=None):
  # N.b.: pool must be a subset of dequeue_mail()
  if sr.get(MAIL_LOCK) and sr.get(MAIL_LOCK) == 'yes':
    return -1
  else:
    sr.set(MAIL_LOCK, 'yes')
    sr.expire(MAIL_LOCK, 60 * 60 * 1) # 1 hour 

  pool = pool or db.dequeue_mail()
  n_sent, n_failed = 0, 0
  for t in pool:
    db.remove_mail(t.get('_id'))
    recpt = t.get('recpt')
    subj = t.get('subj')
    body = t.get('body')
    html = t.get('html')
    if not send_mail(recpt, subj, body, html):
      db.add_troublesome_email_addr(recpt)
      n_failed += 1
    else:
      n_sent += 1

  sr.delete(MAIL_LOCK)
  return n_sent, n_failed



if __name__ == '__main__':
  dequeue_mail()
